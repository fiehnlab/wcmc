package edu.ucdavis.fiehnlab.wcmc.api.rest.stasis4j

import edu.ucdavis.fiehnlab.wcmc.api.rest.stasis4j.client.StasisClient
import org.springframework.context.annotation.{Bean, ComponentScan, Configuration}

@Configuration
@ComponentScan
class Stasis4jConfiguration {
}
